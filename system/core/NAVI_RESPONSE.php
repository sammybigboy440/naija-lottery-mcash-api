<?php

/**
 * Description of NAVI_ERROR
 *
 * @author Egere Samuel C
 */
class NAVI_RESPONSE
{
    //put your code here
    protected function set($key, $val)
    {
        header("$key: $val");
    }
    public function content_type($type)
    {
        $this->set('Content-Type', $type);
    }
}
