<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NAVI_INPUT
 *
 * @author Egere Samuel C
 */
class NAVI_INPUT extends NAVI_ERROR
{

    public function __construct()
    {
        // get the raw POST data
        $row = file_get_contents("php://input");
        // this returns null if not valid json
        if ($row) $this->jsonData = json_decode($row);
    }

    //get POST by name
    public function post($name)
    {
        return isset($_POST[$name]) ? $_POST[$name] : NULL;
    }

    //get GET by name
    public function get($name)
    {
        return isset($_GET[$name]) ? $_GET[$name] : NULL;
    }

    //try GET then POST by name
    public function get_post($name)
    {
        if (isset($_GET[$name])) {
            return trim($_GET[$name]);
        } elseif (isset($_POST[$name])) {
            return trim($_POST[$name]);
        } else {
            return NULL;
        }
    }

    //try POST then GET by name
    public function post_get($name)
    {
        if (isset($_POST[$name])) {
            return trim($_POST[$name]);
        } elseif (isset($_GET[$name])) {
            return trim($_GET[$name]);
        } else {
            return NULL;
        }
    }

    //get php input
    public function json($name = null)
    {
        if ($name == null) return $this->jsonData;
        return isset($this->jsonData->$name) ? $this->jsonData->$name : NULL;
    }
}
