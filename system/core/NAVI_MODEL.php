<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NAVI_MODEL
 *
 * @author Egere Samuel C
 */
class NAVI_MODEL extends NAVI_DB
{
    //database instance
    public $db;
    //database type
    private $dbtype;
    //current database driver
    private $driver;

    //put your code here
    public function __construct()
    {
        parent::__construct();

        $this->driver = "NAVI_" . DB_DRIVER;
        $this->dbtype = DB_TYPE;
        //load the right driver file 
        loader($this->driver, linkGet(SYS_PATH, 'database', $this->dbtype));
        $this->db = new $this->driver;
    }
}
