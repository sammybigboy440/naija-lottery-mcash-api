<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//variable for keeping tack of loaded classes
$loaded = [];
//auto class loader
spl_autoload_register('loader');

router($route);


$log_error = function ($message, $level = "ERROR", $exit = FALSE) {
    $FILE = __FILE__;
    $log = "{$level}: $message" . PHP_EOL;
    // LOG TO OUR DEFAULT LOCATION
    error_log($log);
    if ($exit) {
        exit($log);
    }
};


//entry point function
function router($route)
{
    $get = [];
    $failed = 1; //will be set to 0 once a controller is found.
    $class = '';
    for ($i = (count($route) - 1); $i > 0; $i--) {
        if (!strlen($route[$i])) {
            continue;
        }
        $path = implode(DIR_SEP, array_slice($route, 0, $i));
        $path = linkGet(CONTROLLERS, $path) . DIR_SEP;
        // Set the current directory correctly for CLI requests
        $file = linkGet($path, ucfirst($route[$i]) . '.php');
        if (file_exists($file) && $failed) {
            // require_once $file;
            $failed = 0;
            $class = ucfirst($route[$i]);
            break;
        } else {
            array_unshift($get, $route[$i]);
        }
    }
    if ($failed) {
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", TRUE, 404);
        echo 'Page Not found';
        exit(3); // EXIT_CONFIG
    } else {
        //require the custom definition file
        $custom_environ = linkGet(APP_PATH, 'config', 'customs.php');
        require_once $custom_environ;
        loader($class, $path);
        $controller = new $class;
        $method = !count($get) ? 'index' : method_exists($controller, $get[0]) ? $get[0] : 'index';
        if (count($get) && $method != 'index') array_shift($get);
        if (!method_exists($controller, $method)) {
            //custom error page will be included here in future
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", TRUE, 404);
            echo 'Page Not found ';
            exit(3); // EXIT_CONFIG
        }
        //return a single value to the method if $get is 1 else return the get array
        if (count($get)) {
            $controller->{$method}($get);
        } else {
            $controller->{$method}();
        }
    }
}

//Define the global class loader
function loader($class, $fpath = NULL)
{
    global $loaded;
    $fnd = FALSE; //indicate when class has been found
    //check if an instance of the class exists
    if (!in_array($class, $loaded)) {
        array_push($loaded, $class);
    } else {
        return TRUE;
    }
    //array  of paths to search in
    if ($fpath != NULL) {
        $fpath = linkGet($fpath . DIR_SEP);
        $search = recur_search($fpath, $class . '.php', FALSE);
        if ($search !== FALSE) {
            require_once $search;
            $fnd = TRUE;
            return true;
        }
    } else {
        $paths = [SYS_PATH, MODELS];
        if (defined('CUSTOM_PATH')) $paths[] = CUSTOM_PATH;
        foreach ($paths as $path) {
            $path = linkGet($path . DIR_SEP);
            $search = recur_search($path, $class . '.php');
            if ($search !== FALSE) {
                require_once $search;
                $fnd = TRUE;
                return true;
            }
        }
    }
    //exit the execution with a class failure error
    if (!$fnd) {
        header($_SERVER["SERVER_PROTOCOL"] . " 500 Internal searver eroor", TRUE, 500);

        exit($class . ' not found');
    }
}

/**
 * This function does a recursive search for a class in the system once called
 * It can also be used in a none recursive manner by changing $recur to FALSE
 * 
 * @param type $path
 * @param type $file
 * @return boolean
 */
function recur_search($path, $file, $recur = TRUE)
{
    $file_ext = linkGet($path, $file);
    if (file_exists($file_ext)) {
        return $file_ext;
    } else if ($recur) {
        //find sub directories and run the search
        $content = scandir($path);
        $skip = ['.', '..'];
        //run through the return content array and get the directories only
        foreach ($content as $val) {
            if (in_array($val, $skip)) {
                continue;
            }
            $nPath = linkGet($path, $val) . DIR_SEP;
            if (is_dir($nPath)) {
                $check = recur_search($nPath, $file);
                if ($check !== FALSE) {
                    return $check;
                }
            }
        }
    }
    return FALSE;
}

function linkGet(...$files)
{
    $link = "";
    foreach ($files as $file) {
        $link .= DIR_SEP . $file;
    }
    return  $link = str_replace(DIR_SEP . DIR_SEP, DIR_SEP, $link);
}