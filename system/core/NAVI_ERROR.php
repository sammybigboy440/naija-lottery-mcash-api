<?php

/**
 * Description of NAVI_ERROR
 *
 * @author Egere Samuel C
 */
class NAVI_ERROR
{
    //put your code here

    protected function log_error($message, $level = "ERROR", $exit = FALSE)
    {
        // $FILE = __FILE__;
        $log = "{$level}: " . print_r($message, true);
        // LOG TO OUR DEFAULT LOCATION
        error_log($log);
        if ($exit) {
            //will navigate to error reporting page in future
            if (ENVIRONMENT != 'live') exit($log);
            exit();
        }
    }
    protected function log_die($message, $level = "ERROR")
    {
        $log = "{$level}: $message" . PHP_EOL;
        var_dump($log);
        die;
    }

    protected function panic($message, $level = "ERROR")
    {
        $log = "{$level}: $message" . PHP_EOL;
        throw new Exception($log, 1);
    }
}
