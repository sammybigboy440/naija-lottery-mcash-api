<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NAVI_CONTROLLER
 *
 * @author Egere Samuel C
 */
class NAVI_CONTROLLER extends NAVI_ERROR
{

    public function __construct()
    {
        $this->load = $this;
        $this->session = new NAVI_SESSION;
        $this->input = new NAVI_INPUT;
        $this->response = new NAVI_RESPONSE;
    }

    /**
     * Used for loading models into the controller
     * @param type $classes
     */
    public function model($classes)
    {
        foreach ($classes as $class) {
            $this->load->{$class} = new $class;
        }
    }

    // public function httpState(int $code = null, string $msg = null)
    public function httpState($code = null, $msg = null)
    {
        if ($code == NULL) return http_response_code();

        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

        header("$protocol $code $msg", true, $code);

        $GLOBALS['http_response_code'] = $code;
    }

    public function view($_path, $_data = [], $_return = FALSE)
    {
        //prepare the variables to be used by the page
        foreach ($_data as $key => $val) {
            ${$key} = $val;
        }
        //get the file from the path
        $gfile = explode(DIR_SEP, $_path);
        $file = end($gfile);
        $_c_path = recur_search(str_replace($file, '', VIEWS . $_path), $file . '.php', FALSE);
        //buffer the output so as to enable page content returning
        ob_start();

        if ($_c_path !== FALSE) {
            include $_c_path; //this allows multiple include for the same file so as to cover for templating where needed
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . ' View : ' . $file . ' does not exist', TRUE, 505);
            echo 'View : ' . $file . ' does not exist';
            exit(3);
        }

        if ($_return) {
            $_page = ob_get_contents();
            ob_end_clean();
            return $_page;
        }

        ob_get_flush();
    }
}
