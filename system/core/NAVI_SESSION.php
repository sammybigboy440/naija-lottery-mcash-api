<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NAVI_SESSION
 *
 * @author Egere Samuel C
 */
class NAVI_SESSION
{

    //put your code here
    public function __construct()
    {
        if (!strlen(session_id())) session_start();
    }

    public function get($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : NULL;
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
        return $this;
    }

    public function clear($key)
    {
        unset($_SESSION[$key]);
        return $this;
    }

    public function getall()
    {
        return $_SESSION;
    }
}
