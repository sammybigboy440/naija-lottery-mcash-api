<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDO
 *
 * @author Egere Samuel C
 */
class NAVI_PDO extends NAVI_DB
{

    //the sql string to be executed
    public $sql = '';
    //current database
    private $dbh = '';
    //stores an array of the $key $value pare to be binded
    private $binds = [];
    //for storing the prepared query 
    private $query;
    //this track the status of the last executed SQL
    public $status = FALSE;
    private $dump = false;

    public function __construct()
    {
        parent::__construct();

        //connect to mysql
        $this->connect();
    }

    private function transact($case)
    {
        switch ($case) {
            case 1:
                $this->dbh->beginTransaction();
                $this->log_error('started transaction');
                break;
            case 2:
                $this->dbh->commit();
                $this->log_error('commited transaction');
                break;
            default:
                $this->dbh->rollback();
                $this->log_error('rolledback transaction');
                break;
        }
        return $this;
    }

    public function begin()
    {
        $this->transact(1);
        return $this;
    }
    public function commit()
    {
        $this->transact(2);
        return $this;
    }
    public function rollback()
    {
        $this->transact(3);
        return $this;
    }

    /**
     * This accepts a single column or variable number of columns
     * 
     * @param type $columns
     * @return $this
     */
    public function select(...$columns)
    {
        if (!count($columns)) $this->sql = "SELECT *";
        foreach ($columns as $col) {
            $this->sql .= empty($this->sql) ? "SELECT $col" : ", $col";
        }
        return $this;
    }

    public function insert($table, $pair = [])
    {
        $this->sql .= "INSERT INTO $table";
        if (empty($pair)) {
            $this->log_error("PDO insert was called without data", 'ERROR', TRUE);
        }
        $cols = " (";
        $vals = " (";
        $i = 0;
        foreach ($pair as $col => $val) {
            $cols .= (($i != 0) ? "," : "") . " $col";
            $vals .= (($i != 0) ? "," : "") . " :$col";
            $i++;
        }
        $cols .= " ) VALUES";
        $vals .= " )";
        $this->sql .= $cols . $vals;
        //store the key value pare for binding later
        $this->binds = array_merge($this->binds, $pair);
        return $this;
    }

    public function delete()
    {
        $this->sql .= " DELETE";
        return $this;
    }

    public function from($tbl)
    {
        $this->sql .= " FROM $tbl";
        return $this;
    }

    public function update($tbl)
    {
        $this->sql .= "UPDATE $tbl";
        return $this;
    }

    public function set(...$pair)
    {
        $this->sql .= " SET";
        $keys = array_keys($pair);
        for ($i = 0; $i < count($keys); $i++) {
            $this->sql .= ($i == 0) ? " $keys[$i] = :$keys[$i]" : ", $keys[$i] = :$keys[$i]";
        }
        //store the key value pair for binding later
        array_merge($this->bind, $pair);
        return $this;
    }

    public function where($pair, $join = ['AND'])
    {
        $this->sql .= " WHERE";
        $keys = array_keys($pair);
        for ($i = 0; $i < count($keys); $i++) {
            $sep = $pair[$keys[$i]] == NULL ? 'IS' : '=';
            if ($i == 0) {
                $this->sql .= " $keys[$i] $sep :$keys[$i]";
            } else {
                $this->sql .= ((isset($join[$i])) ? $join[$i] . "" : " AND") . " $keys[$i] $sep :$keys[$i] ";
            }
        }
        //store the key value pair for binding later
        $this->binds = array_merge($this->binds, $pair);
        return $this;
    }

    private function bind($binder = 'bindValue')
    {
        foreach ($this->binds as $key => $val) {

            switch (gettype($val)) {
                case "boolean":
                    $type = PDO::PARAM_BOOL;
                    break;
                case "integer":
                    $type = PDO::PARAM_INT;
                    break;
                case "float":
                case "double":
                case "string":
                    $type = PDO::PARAM_STR;
                    break;
                case "NULL":
                    $type = PDO::PARAM_NULL;
                    break;
                case "array":
                case "object":
                case "case resource":
                case "case resource (closed)":
                    break;
                default:
                    $type = PDO::PARAM_STR;
                    break;
            }
            $this->query->{$binder}(":$key", $val, $type);
        }
        //empty the binds once bind is done
        $this->binds = [];
        return $this;
    }

    public function rowCount()
    {
        if (strlen($this->sql)) $this->execute();
        return $this->query->rowCount();
    }

    public function queryCount()
    {
        $this->sql = "SELECT COUNT(*) AS cnt $this->sql";

        if (strlen($this->sql)) $this->execute();
        $data = $this->fetchOBJ();
        if (property_exists($data, 'cnt')) return $data->cnt;
        return 0;
    }
    /**
     * Returns the ID of the last inserted row or sequence value
     * @param type $name - name of the sequence
     * @return int
     */
    public function LastID($name = NULL)
    {
        return $this->dbh->lastInsertId($name);
    }

    /**
     * Prepares and executes the the SQL
     * @param type $binder
     * @return $this
     */
    public function execute($sql = "", $pair = [], $binder = 'bindValue')
    {
        //check if execute is called directly with with an sql
        $this->sql = !strlen($sql) ? $this->sql : $sql;
        $this->binds = array_merge($this->binds, $pair);
        $this->query = $this->dbh->prepare($this->sql);
        $this->bind($binder);
        $this->status = $this->query->execute();
        //fetch the execution error info
        if (strlen($this->query->errorInfo()[2])) {
            $this->log_error($this->query->errorInfo()[2] . PHP_EOL . $this->sql, 'SQL ERROR');
        }
        //clear $this->sql after bind
        if ($this->dump) $this->log_error($this->sql);
        $this->sql = '';
        return $this;
    }

    /**
     * set dump sql mode
     */
    public function LogMode($mode = true)
    {
        $this->dump = $mode;
        return $this;
    }

    /**
     * Fetch All the results
     * @param type $style - PDO fetch type e.g: PDO::FETCH_BOTH
     */
    public function fetchAll($style = PDO::FETCH_BOTH)
    {
        if (strlen($this->sql)) $this->execute();
        return $this->query->fetchAll($style);
    }

    /**
     * Fetch a row from the results
     * @param type $style - PDO fetch type e.g: PDO::FETCH_BOTH
     */
    public function fetch($style = PDO::FETCH_BOTH)
    {
        if (strlen($this->sql)) $this->execute();
        return $this->query->fetch($style);
    }

    /**
     * Fetch the results as an object
     */
    public function fetchOBJ()
    {
        if (strlen($this->sql)) $this->execute();
        return $this->query->fetchObject();
    }
    /**
     * return the writen sql
     */
    public function getsql()
    {
        return $this->sql;
    }

    /**
     * log the writen sql
     */
    public function logsql()
    {
        $this->log_error($this->sql, "SQL DUMP");
        return $this;
    }

    /**
     * Checks if database connection is opened and open it if not
     */
    protected function connect($dbname = '')
    {
        $dbname = !empty($dbname) ? $dbname : NAVI_DB_NAME;
        $dbname = strlen($dbname) ? ";dbname= $dbname" : "";
        // connection already opened
        if ($this->dbh != null && empty($dbname)) {
            return true;
        } else {
            // create a database connection, using the constants from config/config.php
            try {
                $this->dbh = new PDO('mysql:host=' . NAVI_DB_HOST . $dbname . ';charset=utf8', NAVI_DB_USER, NAVI_DB_PASS);
                return true;
                // If an error is catched, database connection failed
            } catch (PDOException $e) {
                $this->log_error($e->getMessage(), 'SQL ERROR', TRUE);
                return false;
            }
        }
    }
}
