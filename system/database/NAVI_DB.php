<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NAVI_DB
 *
 * @author Egere Samuel C
 */
class NAVI_DB extends NAVI_ERROR
{ //extend error class

    public function __construct()
    {
        //load the config file
        $path = APP_PATH . 'config' . DIR_SEP;
        $config_path = recur_search($path, 'database.php', FALSE);
        if ($config_path != FALSE) {
            require_once $config_path;
        }
    }
}
