<?php

/* 
 * DATABASE CONFIGURATION
 * 
 */

/**
 * specify the type of database your project will be running on
 */
define('DB_TYPE', 'mysql');

/**
 * Select your preferred Database driver
 * possible values include
 * PDO, ---more drivers will be added in future---
 */
define('DB_DRIVER', 'PDO');


/**
 * Provide Connection parameters
 */
define('NAVI_DB_HOST', 'localhost'); // database server address, it could be an ip e.g 192.168.0.1
define('NAVI_DB_USER', ''); // mysql database username
define('NAVI_DB_PASS', ''); // mysql database password
define('NAVI_DB_NAME', '');
