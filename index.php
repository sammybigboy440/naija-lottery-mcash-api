<?php

/**
 * NAVIGATOR FRAMEWORK
 * Built BY Egere Samuel
 */
//set your environment status 'LIVE' 
define('ENVIRONMENT', 'live'); //live or development

//directory seperator
define('DIR_SEP', DIRECTORY_SEPARATOR);
//current directory
define('DIR', dirname(__FILE__) . DIR_SEP);
//path to system files
define('SYS_PATH', DIR . 'system' . DIR_SEP);
//path to the application folder
define('APP_PATH', DIR . 'application' . DIR_SEP);
//path to Controllers
define('CONTROLLERS', APP_PATH . 'controller' . DIR_SEP);
//path to model
define('MODELS', APP_PATH . 'model' . DIR_SEP);
//path to view
define('VIEWS', APP_PATH . 'view' . DIR_SEP);

Init_error_system(ENVIRONMENT);

/**
 * System Error handling.
 * @param type $env
 */
function Init_error_system($env)
{
    switch ($env) {
        case 'developement':
            //managing your errors
            error_reporting(E_ALL); // Error engine - always TRUE!
            ini_set('ignore_repeated_errors', FALSE); // always TRUE
            ini_set('display_errors', TRUE); // Error display - FALSE only in production environment or real server
            break;
        case 'live':
            //managing your errors
            error_reporting(FALSE); // Error engine - always TRUE!
            ini_set('ignore_repeated_errors', TRUE); // always TRUE
            ini_set('display_errors', FALSE); // Error display - FALSE only in production environment or real server
            break;

        default:
            break;
    }
    ini_set('$log_errors', TRUE); // Error logging engine
    ini_set('error_log', APP_PATH . 'log/errors.log'); // Logging file path
    ini_set('$log_errors_max_len', 1024); // Logging file size
}

$request_path = $_SERVER['REQUEST_URI'];

if (isset($request_path) && $request_path != "/") {
    $path = $request_path;
    $route = explode('/', ltrim($request_path));
} else {
    $route = ['Index'];
}
// var_dump(SYS_PATH . 'core' . DIR_SEP . 'NAVIGATOR.php');
// die;
require_once SYS_PATH . 'core' . DIR_SEP . 'NAVIGATOR.php';
